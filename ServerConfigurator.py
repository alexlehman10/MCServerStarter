import tkinter as tk
import os
import subprocess
import urllib.request

window = tk.Tk()
window.title("Acdop100's Minecraft Server Starter")
window.geometry("650x390")


def configwrite2():
    if os.name == 'nt':
        jarname = jarwindow.get()
        minram = minramwindow.get()
        maxram = maxramwindow.get()
        config = open('output_file.bat', "a")
        config.write("java -jar " + jarname + " -Xms" + minram + ' -Xmx' + maxram + " -d64" + "\n")
        config.write("pause")
    else:
        jarname = jarwindow.get()
        minram = minramwindow.get()
        maxram = maxramwindow.get()
        config = open('start.command', "a")
        config.write("#!/bin/bash")
        config.write("java -jar " + jarname + " -Xms" + minram + ' -Xmx' + maxram + " -d64" + "\n")
        config.write("pause")


def configwrite1():
    if os.name == 'nt':
        minram = minramwindow.get()
        maxram = maxramwindow.get()
        config = open('output_file.bat', "a")
        config.write("java -jar " + "minecraft_server.1.11.2.jar" + " -Xms" + minram +
                     ' -Xmx' + maxram + " -d64" + "\n")
        config.write("pause")
    else:
        minram = minramwindow.get()
        maxram = maxramwindow.get()
        config = open('start.command', "a")
        config.write("#!/bin/bash")
        config.write("java -jar " + "minecraft_server.1.11.2.jar" +
                     " -Xms" + minram + ' -Xmx' + maxram + " -d64" + "\n")
        config.write("pause")


def ethcopy():
    if os.name == 'nt':
        subprocess.run(['clip.exe'], input=txt.strip().encode('utf-16'), check=True)
    else:
        process = subprocess.Popen(
            'pbcopy', env={'LANG': 'en_US.UTF-8'}, stdin=subprocess.PIPE)
        process.communicate(txt.encode('utf-8'))


def getserver():
    urllib.request.urlretrieve(
        "https://s3.amazonaws.com/Minecraft.Download/versions/1.12.2/minecraft_server.1.12.2.jar")


def getforge():
    urllib.request.urlretrieve("http://files.minecraftforge.net/maven/net/minecraftforge/forge/"
                               "1.12.2-14.23.0.2491/forge-1.12.2-14.23.0.2491-installer.jar")


minramlbl = tk.Label(window, text="Minimum amount of RAM to use?", font=('Helvetica', 15))
pleaselbl = tk.Label(window, text="For 1g of RAM write 1G, or 500mb write 500M", font=("Helvetica", 11))
minramwindow = tk.Entry(window)

maxramlbl = tk.Label(window, text="Maximum amount of RAM to use?", font=('Helvetica', 15))
please2lbl = tk.Label(window, text="For 1g of RAM write 1G, or 500mb write 500M", font=("Helvetica", 11))
maxramwindow = tk.Entry(window)

serverfile = tk.Button(window, text="Download Minecraft server version 12.2", command=getserver)

forgefile = tk.Button(window, text="Download Forge for version 12.2", command=getforge)

jarlbl = tk.Label(window, text="OR download your own server file, and put the server's .jar filename here",
                  font=('Helvetica', 15))
please3lbl = tk.Label(window, text="Make sure to download the corresponding version of Forge as well if needed",
                      font=("Helvetica", 11))
jarwindow = tk.Entry(window)

tiplbl = tk.Label(window, text="if you are running OS X, type 'chmod a+x' into terminal, drag the start.command after "
                               "it, and then press enter. This will let you run it as a command.",
                  font=("Helvetica", 13), wraplength=580)
donationlbl = tk.Label(window, text="Donate Ethereaum to 0xc81f792CC0D4845CAfB7E077463E6fF3c574a508",
                       font=("Helvetica", 13), wraplength=580)

txt = '0xc81f792CC0D4845CAfB7E077463E6fF3c574a508'

savebtn = tk.Button(window, text="Create startup file", command=configwrite2)

copybtn = tk.Button(window, text="Copy ETH donation address", command=ethcopy)

serverfile.pack()
forgefile.pack()

jarlbl.pack()
please3lbl.pack()
jarwindow.pack()


minramlbl.pack()
pleaselbl.pack()
minramwindow.pack()

maxramlbl.pack()
please2lbl.pack()
maxramwindow.pack()

savebtn.pack()

tiplbl.pack()
donationlbl.pack()
copybtn.pack()

window.mainloop()
